#install the version of node 
wget https://github.com/prometheus/node_exporter/releases/download/v1.2.2/node_exporter-1.2.2.linux-amd64.tar.gz

#extract the file downloaded
tar -xzvf node_exporter-1.2.2.linux-amd64.tar.gz

#Create a user for the node exporter
useradd -rs /bin/false nodeusr

#change directory to node_exporter-1.2.2.linux-amd64 
cd node_exporter-1.2.2.linux-amd64

#Move binary to /usr/local/bin from the extracted package
mv node_exporter /usr/local/bin/

#Create a system service fil for the node exporter
touch /etc/systemd/system/node_exporter.service

#Add the following content to the file
echo -e "[Unit]\nDescription=Node Exporter\nAfter=network.target\n\n[Service]\nUser=nodeusr\nGroup=nodeusr\nType=simple\nExecStart=/usr/local/bin/node_exporter\n\n[Install]\nWantedBy=multi-user.target" >> /etc/systemd/system/node_exporter.service

#reload daemon
systemctl daemon-reload

#enable and start and get status of node_exporter
systemctl enable node_exporter
systemctl start node_exporter
systemctl status node_exporter
